#!/bin/bash

set -xe

workdir="${PWD}"

while [[ $# -gt 0 ]]; do
  case "$1" in
  --codename) shift; codename="$1"; ;;
  --arch) shift; arch="$1"; ;;
  --qemu-suite) shift; qemu_suite="$1"; ;;
  --qemu-arch) shift; qemu_arch="$1"; ;;
  --timestamp) shift; timestamp="$1"; ;;
  --staging-tag-prefix) shift; staging_tag_prefix="$1"; ;;
  *)
    echo "Unknown argument '$1'. Exit." >&2; exit 1; ;;
  esac
  shift
done

[[ -n "${codename}" ]] || { echo "Unspecified codename. Exit." >&2; exit 1; }
[[ -n "${arch}" ]] || { echo "Unspecified arch. Exit." >&2; exit 1; }
[[ -n "${qemu_suite}" ]] || qemu_suite="${codename}"
[[ "${qemu_arch}" != "no" ]] || qemu_arch=
[[ -n "${timestamp}" ]] || timestamp="today 00:00:00"
[[ -n "${staging_tag_prefix}" ]] || { echo "Unspecified docker staging tag prefix. Exit." >&2; exit 1; }

export out="${workdir}/output/rootfs/${codename}:${arch}"
mkdir -p "${out}"

base_url="http://deb.debian.org/debian/dists/${codename}"
archive_url="http://archive.debian.org/debian/dists/${codename}"
ports_url="http://deb.debian.org/debian-ports/dists/${codename}"
if wget --no-verbose --output-document=/dev/null "${base_url}/Release"; then
  if ! wget --no-verbose --output-document=/dev/null \
      "${base_url}/main/binary-${arch}/Release"; then
    if wget --no-verbose --output-document=/dev/null \
        "${archive_url}/main/binary-${arch}/Release"; then
      is_eol=true
    elif wget --no-verbose --output-document=/dev/null \
        "${ports_url}/main/binary-${arch}/Release"; then
      is_ports=true
    else
      echo "Don't know how to build ${codename}/${arch}." >&2
      exit 1
    fi
  fi
else
  is_eol=true
fi

if test -n "${qemu_arch}"; then
  cid=$(docker create --rm --privileged "vicamo/binfmt-qemu:${qemu_suite}")
  docker cp "${cid}:/usr/bin/qemu-${qemu_arch}-static" .
  docker start "${cid}"
fi

git clone https://github.com/vicamo/docker_debuerreotype.git debuerreotype
"${workdir}/debuerreotype/build.sh" \
    ${is_eol:+--eol} \
    ${is_ports:+--ports} \
    --arch="${arch}" \
    ${qemu_arch:+--qemu --qemu-suite="${qemu_suite}"} \
    "${out}" \
    "${codename}" \
    "${timestamp}"

ts=$(ls -1 "${out}")
d="${out}/${ts}/${arch}/${codename}"
for flavor in "" slim sbuild; do
  staging_tag="${staging_tag_prefix}${codename}-${arch}${flavor:+-${flavor}}"
  d_flavor="${d}${flavor:+/${flavor}}"
  if test -n "${qemu_arch}"; then
    cp "qemu-${qemu_arch}-static" "${d_flavor}"
    sed -e "s,@QEMU_ARCH@,${qemu_arch},g" Dockerfile.qemu > "${d_flavor}"/Dockerfile
  else
    cp Dockerfile "${d_flavor}"
  fi
  docker build --tag "${staging_tag}" "${d_flavor}"
done

# Sanity check
for flavor in "" -slim -sbuild; do
  staging_tag="${staging_tag_prefix}${codename}-${arch}${flavor}"
  docker run --rm "${staging_tag}" apt-get update --quiet
done
